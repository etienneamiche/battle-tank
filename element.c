#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "texture.h"
#include "element.h"
/*-------
  BALLE
--------*/

listeBalle *Init_ListeBalle()
{
  listeBalle *l = malloc(sizeof(listeBalle));
  l->debut = NULL;
  l->fin = NULL;
  l->taille = 0;
  return l;
}
int ListeBalle_vide(listeBalle *l)
{
  if (l->debut == NULL && l->fin == NULL)
  {
    return 1;
  }
  return 0;
}
balle *Creer_Balle(tank *t, int direction)
{
  balle *b = malloc(sizeof(balle));

  /* On assigne la valeur au nouvel élément */
  b->direction = direction;

  //En fonction de la direction on creer la balle devant le tank et au milieu pour simuler la sortit du canon
  if (direction == 0)
  { //BAS
    b->corp.x = t->corp.x + 10;
    b->corp.y = t->corp.y + 20;
  }
  else if (direction == 1)
  { //DROITE
    b->corp.x = t->corp.x + 20;
    b->corp.y = t->corp.y + 10;
  }
  else if (direction == 2)
  { //HAUT
    b->corp.x = t->corp.x + 10;
    b->corp.y = t->corp.y;
  }
  else if (direction == 3)
  { //GAUCHE
    b->corp.x = t->corp.x;
    b->corp.y = t->corp.y + 10;
  }

  b->corp.h = 10;
  b->corp.w = 10;
  b->niveau = t->niveau;
  return b;
}

void Ajouter_Balle_Fin(listeBalle *liste, tank *t, int direction, int niveau)
{
  balle *b = Creer_Balle(t, direction);
  /* On ajoute en fin, donc aucun élément ne va suivre */
  b->nxt = NULL;

  //Si la liste est vide
  if (ListeBalle_vide(liste) == 1)
  {
    liste->debut = b;
    liste->fin = b;
    b->index = 0;
    liste->taille += 1;
    return;
  }
  //Sinon on rajoute en fin de liste
  else
  {
    balle *temp = liste->fin;
    b->index = temp->index + 1;
    liste->taille += 1;
    temp->nxt = b;
    liste->fin = b;
    return;
  }
}

//Supression des element dans une liste de balle

void Supprimer_Balle(listeBalle *list, int index)
{
  if (ListeBalle_vide(list) == 1)
    return; //si liste vide on ne fait rien

  balle *previous;
  balle *tmp;
  if (list->taille == 1)
  {
    tmp = list->debut;
    list->debut = NULL;
    list->fin = NULL;
    list->taille = 0;
    free(tmp);
    return;
  }

  if (list->debut->index == index) // Verifie le debut de liste, cas particulier
  {
    tmp = list->debut;
    list->debut = tmp->nxt;
    tmp->nxt = NULL;
    list->taille -= 1;
    free(tmp);
    return;
  }

  previous = list->debut;
  tmp = previous->nxt; // le cas n est gere on se place donc sur le cas n+1

  while (tmp != NULL)
  { // On Mouline est on supprime si on trouve le obus_t
    if (tmp->index == index)
    {

      previous->nxt = tmp->nxt;
      list->taille -= 1;
      free(tmp);
      if (previous->nxt == NULL)
      {
        list->fin = previous;
      }
      return;
    }
    previous = tmp; // pour ce souvenir dans la prochaine iteration du precedent
    tmp = tmp->nxt;
  }
  return;
}
//Pour tirer on ajoute une balle dans la liste
void shoot(tank *t, int direction)
{
  Ajouter_Balle_Fin(t->balles, t, direction, t->niveau);
}

//Permet de deplacer une balle dans le bon sens en fonction de la direction
void bouger_balle_direction(balle *b, int direction)
{
  //BAS
  if (direction == 0)
  {
    b->corp.y += 10;
  }

  //DROITE
  else if (direction == 1)
  {
    b->corp.x += 10;
  }
  //HAUT
  else if (direction == 2)
  {
    b->corp.y -= 10;
  }
  //GAUCHE
  else if (direction == 3)
  {
    b->corp.x -= 10;
  }
}

//obtenir la position du caractere C afin de detecter le tank a suprmier dans la liste
position Get_Tank_Pos(char pos, position charPos)
{
  position p;
  switch (pos)
  {
  case 'C':
    p = charPos;
    break;
  case 'D':
    p.y = charPos.y;
    p.x = charPos.x - 1;
    break;
  case 'E':
    p.y = charPos.y;
    p.x = charPos.x - 2;
    break;
  case 'F':
    p.y = charPos.y - 1;
    p.x = charPos.x;
    break;
  case 'G':
    p.y = charPos.y - 1;
    p.x = charPos.x - 1;
    break;
  case 'H':
    p.y = charPos.y - 1;
    p.x = charPos.x - 2;
    break;
  case 'I':
    p.y = charPos.y - 2;
    p.x = charPos.x;
    break;
  case 'J':
    p.y = charPos.y - 2;
    p.x = charPos.x - 1;
    break;
  case 'K':
    p.y = charPos.y - 2;
    p.x = charPos.x - 2;
    break;

  default:
    break;
  }
  p.x *= 10;
  p.y *= 10;
  return p;
}
int ListeTank_vide(listeTank *l)
{
  if (l->debut == NULL && l->fin == NULL)
  {
    return 1;
  }
  return 0;
}

void Effacer_Tank_Sur_Carte(SDL_Rect *corp, char **map)
{
  position p;
  p.x = (corp->x) / 10;
  p.y = (corp->y) / 10;

  for (int i = 0; i < 3; i++)
  {
    for (int j = 0; j < 3; j++)
    {
      map[p.y + i][p.x + j] = '.';
    }
  }
}

//Reduit de 1 le niveau du tank. Si le niveau est egale a -1 on supprime le tank
void Toucher_Tank(listeTank *list, position pos, char **mapDynamique)
{
  if (ListeTank_vide(list) == 1)
    return; //si liste vide on ne fait rien

  tank *previous;
  tank *tmp;
  if (list->taille == 1)
  {
    tmp = list->debut;
    tmp->niveau -= 1;
    if (tmp->niveau == -1)
    {
      Effacer_Tank_Sur_Carte(&list->debut->corp, mapDynamique);
      list->debut = NULL;
      list->fin = NULL;
      list->taille = 0;
      free(tmp);
      return;
    }
    return;
  }

  if (list->debut->corp.x == pos.x && list->debut->corp.y == pos.y) // Verifie le debut de liste, cas particulier
  {
    tmp = list->debut;
    tmp->niveau -= 1;
    if (tmp->niveau == -1)
    {
      Effacer_Tank_Sur_Carte(&tmp->corp, mapDynamique);
      list->debut = tmp->nxt;
      tmp->nxt = NULL;
      list->taille -= 1;
      free(tmp);
      return;
    }
    return;
  }

  previous = list->debut;
  tmp = previous->nxt; // le cas n est gere on se place donc sur le cas n+1

  while (tmp != NULL)
  { // On Mouline est on supprime si on trouve le obus_t
    if (tmp->corp.x == pos.x && tmp->corp.y == pos.y)
    {
      tmp->niveau -= 1;
      if (tmp->niveau == -1)
      {
        Effacer_Tank_Sur_Carte(&tmp->corp, mapDynamique);
        previous->nxt = tmp->nxt;
        list->taille -= 1;
        free(tmp);
        if (previous->nxt == NULL)
        {
          list->fin = previous;
        }
        return;
      }
      return;
    }
    previous = tmp; // pour ce souvenir dans la prochaine iteration du precedent
    tmp = tmp->nxt;
  }
  return;
}
//Bouger les balle en fonction des obstables
int bouger_balles(listeBalle *liste, char **map, listeTank *listeT, char **mapDynamique, int joueur)
{
  if (ListeBalle_vide(liste) == 0)
  {
    balle *b;
    b = liste->debut;
    while (b != NULL)
    {

      position pos;
      position p;
      pos.x = b->corp.x;
      pos.y = b->corp.y;

      int direction = b->direction;

      if (b->corp.x < 900 && b->corp.x > 0 && b->corp.y < 900 && b->corp.x > 0)
      {

        //BAS
        if (direction == 0)
        {

          p.x = pos.x / 10;
          p.y = pos.y / 10 + 1;
        }

        //DROITE
        else if (direction == 1)
        {

          p.x = pos.x / 10 + 1;
          p.y = pos.y / 10;
        }
        //HAUT
        else if (direction == 2)
        {

          p.x = pos.x / 10;
          p.y = pos.y / 10 - 1;
        }
        //GAUCHE
        else if (direction == 3)
        {
          p.x = pos.x / 10 - 1;
          p.y = pos.y / 10;
        }

        //On verifie sur les deux cartes si la case devant libre
        if (map[p.x][p.y] == '.' && mapDynamique[p.y][p.x] == '.')
        {
          bouger_balle_direction(b, direction);
        }
        else
        {
          Supprimer_Balle(liste, b->index);
          switch (map[p.x][p.y])
          {
          case 'b':
            map[p.x][p.y] = '.';

            if(map[p.x+1][p.y] == 'b'){
               map[p.x+1][p.y] = '.';
            }
            if(map[p.x-1][p.y] == 'b'){
               map[p.x-1][p.y] = '.';
            }
            if(map[p.x][p.y+1] == 'b'){
               map[p.x][p.y+1] = '.';
            }
            if(map[p.x][p.y-1] == 'b'){
               map[p.x][p.y-1] = '.';
            }
            
            break;
          case 'B':
            if (b->niveau == 2)
            {
              map[p.x][p.y] = '.';
            if(map[p.x+1][p.y] == 'B' || map[p.x+1][p.y] == 'b'){
               map[p.x+1][p.y] = '.';
            }
            if(map[p.x-1][p.y] == 'B' || map[p.x+1][p.y] == 'b'){
               map[p.x-1][p.y] = '.';
            }
            if(map[p.x][p.y+1] == 'B' || map[p.x+1][p.y] == 'b'){
               map[p.x][p.y+1] = '.';
            }
            if(map[p.x][p.y-1] == 'B' || map[p.x+1][p.y] == 'b'){
               map[p.x][p.y-1] = '.';
            }
            }
            break;
          case 'T':
              return 1;
              break;
              
          default:
            break;
          }

          position tankPos;
          if (joueur == 1)
          {

            switch (mapDynamique[p.y][p.x])
            {
            case 'C':
            case 'D':
            case 'E':
            case 'F':
            case 'G':
            case 'H':
            case 'I':
            case 'J':
            case 'K':

              tankPos = Get_Tank_Pos(mapDynamique[p.y][p.x], p);

              Toucher_Tank(listeT, tankPos, mapDynamique);

              break;

            default:
            break;
         
            }
          } else {
            switch (mapDynamique[p.y][p.x])
            {
            case 'P':
            

              tankPos = Get_Tank_Pos(mapDynamique[p.y][p.x], p);

              Toucher_Tank(listeT, tankPos, mapDynamique);

              break;
            
            

            default:
              break;
            }
          }
        }
      }
      else
      {
        Supprimer_Balle(liste, b->index);
      }
      b = b->nxt;
    }
    free(b);
  }
  return 0;
}
void afficher_balles(listeBalle *liste,SDL_Texture ** textures, SDL_Renderer *renderer,int joueur)
{


  SDL_Rect clip_balle[3] = {{ 0, 0, BALLE_SPRITE_SHEET_HEIGHT, BALLE_SPRITE_SHEET_WIDTH/3 } ,
                            { (BALLE_SPRITE_SHEET_WIDTH/3), 0, BALLE_SPRITE_SHEET_HEIGHT, BALLE_SPRITE_SHEET_WIDTH/3 } ,
                            { (BALLE_SPRITE_SHEET_WIDTH/3)*2, 0, BALLE_SPRITE_SHEET_HEIGHT, BALLE_SPRITE_SHEET_WIDTH/3 } } ;
  if (ListeBalle_vide(liste) == 0)
  {
    balle *b = malloc(sizeof(balle));
    b = liste->debut;
    while (b != NULL)
    { 
      SDL_SetRenderTarget(renderer, textures[0]);
      if(joueur == 0){
        if(b->niveau == 2){
          SDL_RenderCopy(renderer,textures[0],&clip_balle[0],&b->corp);
        }
        else{
          SDL_RenderCopy(renderer,textures[0],&clip_balle[1],&b->corp);
        }
      }
      else{
        SDL_RenderCopy(renderer,textures[0],&clip_balle[2],&b->corp);
      }
      SDL_SetRenderTarget(renderer, NULL);
      b = b->nxt;

    }
    free(b);
  }
}

/*------
  TANK
-------*/
//inserer dans une autre matrice la position d'un tank
void Remplire_Tank_Sur_Carte(SDL_Rect *corp, char **mapDynamique, int joueur)
{

  /* 
  Le tank est représenté sur la carte par une uite de lettre (plus simple pour supprimer un tank en fonction de sa position si jamais deux tank sont collé)
  |C|D|E|
  |F|G|H|
  |I|J|K|

  un joueur est représenté par 
  |P|P|P|
  |P|P|P|
  |P|P|P|
  En fonction de la lettre detecté par la balle on peut remonter jusqu'a C qui correspon a la position du tank et le supprimer dans la liste  
  Si une balle tombre sur P on arrette la partie

  */

  position p;
  p.x = (corp->x) / 10;
  p.y = (corp->y) / 10;

  int c = 'C';
  int count = 0;
  //si on crée un enemy
  if (joueur == 0)
  {
    for (int i = 0; i < 3; i++)
    {
      for (int j = 0; j < 3; j++)
      {
        mapDynamique[p.y + i][p.x + j] = (char)(c + count);
        count++;
      }
    }
  }
  else // sinon on place le caractere P pour un joueur
  {
    for (int i = 0; i < 3; i++)
    {
      for (int j = 0; j < 3; j++)
      {
        mapDynamique[p.y + i][p.x + j] = 'P';
      }
    }
  }
}

/*
  Cree un tank avec:
  -une position x,y,hauteur,largeur(h,w)
  -une direction
  -un corp
  -une texture
  -une liste de balles
  -inserer le tank dans la carte dynamique
  retourne un tank
*/

tank *Creer_Tank(int posX, int posY, int direction, int niveau, char **map, int joueur, listeBalle *listeballe)
{

  tank *t = malloc(sizeof(tank));
  ;

  t->corp.x = posX;
  t->corp.y = posY;
  t->corp.h = 30;
  t->corp.w = 30;

  t->direction = direction;
  t->niveau = niveau;
  /* IMAGE
  t.surface = IMG_Load(SPRITE_SHEET);
  t.texture = SDL_CreateTextureFromSurface(renderer, t.surface);

  SDL_Rect sprites[4];
  fill_tank_player_sprites(sprites);

  t.sprites[0] = sprites[0];
  t.sprites[1] = sprites[1];
  t.sprites[2] = sprites[2];
  t.sprites[3] = sprites[3];
  */
  t->balles = listeballe;

  Remplire_Tank_Sur_Carte(&t->corp, map, joueur);

  return t;
}

/* 
Bouger le tank et gerer les colisions
*/

//deplacer un tank en fonction des obstacles
void bouger_tank_direction(tank *t, int direction)
{
  t->direction = direction;
  //BAS
  if (direction == 0)
  {
    t->corp.y += 10;
  }

  //DROITE
  else if (direction == 1)
  {
    t->corp.x += 10;
  }

  //HAUT
  else if (direction == 2)
  {
    t->corp.y -= 10;
  }
  
  //GAUCHE
  else if (direction == 3)
  {
    t->corp.x -= 10;
  }
}

void Bouger_Tank(tank *t, int direction, char **map, char **mapDynamique, int joueur)
{

  int posX = t->corp.x;
  int posY = t->corp.y;
  position p1, p2, p3;
  //BAS
  if (direction == 0)
  {

    p1.x = posX / 10,
    p1.y = posY / 10 + 3;

    p2.x = posX / 10 + 1,
    p2.y = posY / 10 + 3;

    p3.x = posX / 10 + 2,
    p3.y = posY / 10 + 3;
  }
  //DROITE
  else if (direction == 1)
  {

    p1.x = posX / 10 + 3,
    p1.y = posY / 10;

    p2.x = posX / 10 + 3,
    p2.y = posY / 10 + 1;

    p3.x = posX / 10 + 3,
    p3.y = posY / 10 + 2;
  }
  //HAUT
  else if (direction == 2)
  {

    p1.x = posX / 10,
    p1.y = posY / 10 - 1;

    p2.x = posX / 10 + 1,
    p2.y = posY / 10 - 1;

    p3.x = posX / 10 + 2,
    p3.y = posY / 10 - 1;
  }
  //GAUCHE
  else if (direction == 3)
  {

    p1.x = posX / 10 - 1,
    p1.y = posY / 10;

    p2.x = posX / 10 - 1,
    p2.y = posY / 10 + 1;

    p3.x = posX / 10 - 1,
    p3.y = posY / 10 + 2;
  }


  //On verifie sur les deux cartes si la case devant libre
  if (map[p1.x][p1.y] == '.' && map[p2.x][p2.y] == '.' && map[p3.x][p3.y] == '.' && mapDynamique[p1.y][p1.x] == '.' && mapDynamique[p2.y][p2.x] == '.' && mapDynamique[p3.y][p3.x] == '.')
  {
    Effacer_Tank_Sur_Carte(&t->corp, mapDynamique);
    bouger_tank_direction(t, direction);
    Remplire_Tank_Sur_Carte(&t->corp, mapDynamique, joueur);
  }
}

listeTank *Init_ListeTank()
{
  listeTank *l = malloc(sizeof(listeTank));
  l->debut = NULL;
  l->fin = NULL;
  l->taille = 0;
  return l;
}

void Ajouter_Tank_Fin(int posX, int posY, listeTank *liste, int niveau, char **mapDynamique, listeBalle *listeballe)
{
  tank *t = Creer_Tank(posX, posY, 0, 0, mapDynamique, 0, listeballe);
  /* On ajoute en fin, donc aucun élément ne va suivre */
  t->nxt = NULL;

  //Si la liste est vide
  if (ListeTank_vide(liste) == 1)
  {
    liste->debut = t;
    liste->fin = t;
    t->index = 0;
    t->niveau = niveau;
    liste->taille += 1;
    return;
  }
  //Sinon on rajoute en fin de liste
  else
  {
    tank *temp = liste->fin;
    t->index = temp->index + 1;
    t->niveau = niveau;
    liste->taille += 1;
    temp->nxt = t;
    liste->fin = t;
    return;
  }
}

void afficher_tanks(listeTank *liste, SDL_Renderer *renderer,SDL_Texture ** textures,int joueur)
{
  SDL_Rect clip_tank_leger[4] = {
                            { 0, (TANK_SPRITE_SHEET_HEIGHT/4)*2, TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } ,//B
                            { 0, (TANK_SPRITE_SHEET_HEIGHT/4), TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } ,//D
                            { 0, 0, TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } ,//H
                            { 0, (TANK_SPRITE_SHEET_HEIGHT/4)*3, TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } //G
                          };
  SDL_Rect clip_tank_moyen[4] = {
                            { (TANK_SPRITE_SHEET_HEIGHT/4), (TANK_SPRITE_SHEET_HEIGHT/4)*2, TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } ,//B
                            { (TANK_SPRITE_SHEET_HEIGHT/4), (TANK_SPRITE_SHEET_HEIGHT/4), TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } ,//D
                            { (TANK_SPRITE_SHEET_HEIGHT/4), 0, TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } ,//H
                            { (TANK_SPRITE_SHEET_HEIGHT/4), (TANK_SPRITE_SHEET_HEIGHT/4)*3, TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } //G
                          };
   SDL_Rect clip_tank_lourd[4] = {
                            { (TANK_SPRITE_SHEET_HEIGHT/4)*2, (TANK_SPRITE_SHEET_HEIGHT/4)*2, TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } ,//B
                            { (TANK_SPRITE_SHEET_HEIGHT/4)*2, (TANK_SPRITE_SHEET_HEIGHT/4), TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } ,//D
                            { (TANK_SPRITE_SHEET_HEIGHT/4)*2, 0, TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } ,//H
                            { (TANK_SPRITE_SHEET_HEIGHT/4)*2, (TANK_SPRITE_SHEET_HEIGHT/4)*3, TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } //G
                          };
  SDL_Rect clip_tank_joueur[4] = {
                            { (TANK_SPRITE_SHEET_HEIGHT/4)*3, (TANK_SPRITE_SHEET_HEIGHT/4)*2, TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } ,//B
                            { (TANK_SPRITE_SHEET_HEIGHT/4)*3, (TANK_SPRITE_SHEET_HEIGHT/4), TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } ,//D
                            { (TANK_SPRITE_SHEET_HEIGHT/4)*3, 0, TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } ,//H
                            { (TANK_SPRITE_SHEET_HEIGHT/4)*3, (TANK_SPRITE_SHEET_HEIGHT/4)*3, TANK_SPRITE_SHEET_HEIGHT/4, TANK_SPRITE_SHEET_WIDTH/4 } //G
                          };


  if (ListeTank_vide(liste) == 0)
  {
    tank *t = malloc(sizeof(tank));
    t = liste->debut;
    while (t != NULL)
    {
      
      SDL_SetRenderTarget(renderer, textures[1]);

      if(joueur == 0){

        if(t->niveau == 0){
          SDL_RenderCopy(renderer,textures[1],&clip_tank_leger[t->direction],&t->corp);
        }
        else if(t->niveau == 1)
        {
          SDL_RenderCopy(renderer,textures[1],&clip_tank_moyen[t->direction],&t->corp);
        }
        else
        {
          SDL_RenderCopy(renderer,textures[1],&clip_tank_lourd[t->direction],&t->corp);
        }
      }
      else{
        SDL_RenderCopy(renderer,textures[1],&clip_tank_joueur[t->direction],&t->corp);
      }

      SDL_SetRenderTarget(renderer, NULL);
      

      t = t->nxt;
    }
    free(t);
  }
}

void Bouger_Tank_Liste(listeTank *l, char **map, char **mapDynamique)
{
  int direction, changeDirection;
  if (ListeTank_vide(l) == 0)
  {
    tank *t = malloc(sizeof(tank));
    t = l->debut;
    //On parcour tout les tanks
    while (t != NULL)
    {
      changeDirection = rand() % 6;
      //Ici le tank a 5 chance sur 6 de continuer dans la meme direction ou de tirer
      if (changeDirection != 0)
      {
        if (changeDirection == 1)
        {
          shoot(t, t->direction);
        }
        else
        {
          Bouger_Tank(t, t->direction, map, mapDynamique, 0);
        }
      }
      // Ici on va decider si le tank change de direction ou reste sur place
      else
      {
        direction = rand() % 8;
        if (direction < 4)
        {
          t->direction = direction;
          Bouger_Tank(t, t->direction, map, mapDynamique, 0);
        } else if (direction >=4 && direction <= 7){
          t->direction = 0;
          Bouger_Tank(t, t->direction, map, mapDynamique, 0);
        }
      }
      t = t->nxt;
    }
    free(t);
  }
}




int Test_Carte_libre(char **mapDynamique, int posX, int posY)
{
  for (int i = 0; i < 3; i++)
  {
    for (int j = 0; j < 3; j++)
    {
      if (mapDynamique[posY + j][posX + i] != '.')
      {
        return 0;
      }
    }
  }
  return 1;
}
void Apparaitre_Tank_Enemy_Sur_Map(int *tabTank, listeTank *liste, int index, char **mapDynamique, listeBalle *listeballe)
{
  int position = rand() % 2;
  if (position == 1){
    Ajouter_Tank_Fin(10, 10, liste, tabTank[index], mapDynamique, listeballe);
  } else {
  Ajouter_Tank_Fin(800, 10, liste, tabTank[index], mapDynamique, listeballe);
  }
}