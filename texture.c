#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include "texture.h"


void fill_tank_player_sprites(SDL_Rect clip[4]){

  //DOWN
  clip[0].x=0;
  clip[0].y=0;
  clip[0].w=TANK_SPRITE_SHEET_WIDTH/12;
  clip[0].h=TANK_SPRITE_SHEET_HEIGHT/8;

  //RIGHT
  clip[1].x=0;
  clip[1].y=TANK_SPRITE_SHEET_HEIGHT/4;
  clip[1].w=TANK_SPRITE_SHEET_WIDTH/12;
  clip[1].h=TANK_SPRITE_SHEET_HEIGHT/8;

  //UP
  clip[2].x=0;
  clip[2].y=(TANK_SPRITE_SHEET_HEIGHT/8)*3;
  clip[2].w=TANK_SPRITE_SHEET_WIDTH/12;
  clip[2].h=TANK_SPRITE_SHEET_HEIGHT/8;
  
  //LEFT
  clip[3].x=0;
  clip[3].y=(TANK_SPRITE_SHEET_HEIGHT/8);
  clip[3].w=TANK_SPRITE_SHEET_WIDTH/12;
  clip[3].h=TANK_SPRITE_SHEET_HEIGHT/8;
    
}

void Init_Texture(SDL_Surface * surface,SDL_Renderer * renderer,SDL_Texture ** textures){
  
  //Chargement de la texture des balles
  surface = IMG_Load(BALLE_SPRITE_SHEET);
  textures[0] = SDL_CreateTextureFromSurface(renderer,surface);

  //Chargement de la texture des tanks
  surface = IMG_Load(TANK_SPRITE_SHEET);
  textures[1] = SDL_CreateTextureFromSurface(renderer,surface);

  surface = IMG_Load(OBJECT_SPRITE_SHEET);
  textures[2] = SDL_CreateTextureFromSurface(renderer,surface);
  
  
  
}




  