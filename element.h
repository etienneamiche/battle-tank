#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


/*-----------
    BALLE
-----------*/

typedef struct balle balle;
struct balle
{   
    int index;
    int direction;
    int niveau; //0: leger 1:moyen 2:lourd
    SDL_Rect corp;
    SDL_Texture * texture;
    balle *nxt;
};

typedef struct listeBalle listeBalle;
struct listeBalle{
    balle * debut;
    balle * fin;
    int taille;
};








/*-----------
    TANK
-----------*/

typedef struct tank tank;
struct tank{

    int direction;
    int niveau; //0:leger ,1:moyen, 2:lourd
    int index;
    SDL_Rect corp;
    
    SDL_Texture * texture;    
    SDL_Rect sprites[4];
    listeBalle * balles;
    tank *nxt;

};

typedef struct listeTank listeTank;
struct listeTank{
    tank * debut;
    tank * fin;
    int taille;
};

    
typedef struct{
    
    int x;
    int y;

} position;



