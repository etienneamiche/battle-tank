#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include "texture.h"


// Allocation dynamique d'un tableau a deux dimensions
char **Allocation_Dynamique_Matrice(int lignes, int colonnes)
{
  char **map = malloc(lignes * sizeof(char *));
  for (int i = 0; i < lignes; i++)
  {
    map[i] = malloc(colonnes * sizeof(char));
  }
  return map;
}


//remplissage du tableau a deux dimenssions avec le fichier text "map" en entrée
void Remplir_Carte(int lignes, int colonnes, char **map)
{
  char *mapFile = "map";
  char caractere;

  FILE *file;

  file = fopen(mapFile, "r");
  if (file)
  {
    for (int l = 0; l < lignes; l++){
      for (int c = 0; c < colonnes; c++){
        if (( (caractere = getc(file)) != EOF ) ){
          if(caractere > 31 && caractere < 123){
            map[c][l] = caractere;
          } else {
            c--;
          }
        }
      }
    }
    fclose(file);
  }
}

void Remplir_Carte_Dynamique(int lignes, int colonnes, char **map)
{
    for (int l = 0; l < lignes; l++){
      for (int c = 0; c < colonnes; c++){
      map[c][l] ='.';
      
      }
    }
}


/*
creation de la carte
    - allocation dynamique
    - remplissage
*/
char **Creer_Carte(int lignes, int colonnes)
{

  char ** map = Allocation_Dynamique_Matrice(lignes, colonnes);
  Remplir_Carte(lignes, colonnes, map);
  return map;
}

void Afficher_Carte_Dynamique(char **map){

  for (int l = 0; l < 90; l++){
    for (int c = 0; c < 90; c++){
      fprintf(stdout,"%c",map[l][c]);
    }
  fprintf(stdout,"\n");

  }

}
/* Affichage Carte en fonction des caractère rencontré */
void Afficher_Carte(SDL_Renderer *renderer, int lignes, int colonnes, char **map, SDL_Texture ** textures)
{
  char caractere;

  SDL_Rect clip_object[4] = {
                            { 0, 0, OBJECT_SPRITE_SHEET_HEIGHT, OBJECT_SPRITE_SHEET_WIDTH/4 } ,
                            { (OBJECT_SPRITE_SHEET_WIDTH/4), 0, OBJECT_SPRITE_SHEET_HEIGHT, OBJECT_SPRITE_SHEET_WIDTH/4 } ,
                            { (OBJECT_SPRITE_SHEET_WIDTH/4)*2, 0, OBJECT_SPRITE_SHEET_HEIGHT, OBJECT_SPRITE_SHEET_WIDTH/4 } ,
                            { (OBJECT_SPRITE_SHEET_WIDTH/4)*3, 0, OBJECT_SPRITE_SHEET_HEIGHT, OBJECT_SPRITE_SHEET_WIDTH/4 }
                            } ;

  for (int l = 0; l < lignes; l++){
    for (int c = 0; c < colonnes; c++){
      caractere = map[l][c];

      SDL_Rect rect;
      rect.x = l * 10;
      rect.y = c * 10;
      rect.w = 10;
      rect.h = 10;

      switch (caractere)
      {
      case 'W':
        SDL_SetRenderTarget(renderer, textures[2]);
        SDL_RenderCopy(renderer,textures[2],&clip_object[2],&rect);
        SDL_SetRenderTarget(renderer, NULL);
        
        break;
      case '.':
        /* code */
        break;
      case 'b':
        SDL_SetRenderTarget(renderer, textures[0]);
        SDL_RenderCopy(renderer,textures[2],&clip_object[0],&rect);
        SDL_SetRenderTarget(renderer, NULL);
        break;
      case 'B':
        SDL_SetRenderTarget(renderer, textures[2]);
        SDL_RenderCopy(renderer,textures[2],&clip_object[1],&rect);
        SDL_SetRenderTarget(renderer, NULL);
        break;
      case 'T':
        SDL_SetRenderTarget(renderer, textures[2]);
        SDL_RenderCopy(renderer,textures[2],&clip_object[3],&rect);
        SDL_SetRenderTarget(renderer, NULL);
        break;

      default:
        break;
      }
    }
  }
}