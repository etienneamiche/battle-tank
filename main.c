#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "carte.c"
#include "element.c"
#include "texture.c"


/* COMPILATION

gcc -Wall main.c -o BattleTank `sdl2-config --cflags --libs` -lSDL2_image -lSDL2_mixer

*/

//ECRAN D'ACCEUIL
void MENU1(SDL_Window *window, SDL_Renderer *renderer,SDL_Surface * surface)
{
    SDL_Event event;
    int QUIT = 0;
    
    SDL_Texture *t;
    SDL_Rect rect;
    rect.x = 0;
    rect.y = 0;
    rect.w = WINDOW_WIDTH;
    rect.h = WINDOW_HEIGHT;

    surface = IMG_Load("image/menu1.jpg");
    t = SDL_CreateTextureFromSurface(renderer, surface);

    while (QUIT == 0)
    {

        while (SDL_PollEvent(&event))
        {

            if (event.type == SDL_QUIT)
            {
                QUIT = 1;
            }

            if (event.type == SDL_KEYDOWN)
            {
                switch (event.key.keysym.sym)
                {
                case SDLK_SPACE:
                    QUIT = 1;
                    break;

                default:
                    break;
                }
            }
        }
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, t, &rect, &rect);
        SDL_RenderPresent(renderer);
    }


      
}

//ECRAN SELECTION DE DIFFICULTÉ
//Return 1 pour facile et 0 pour difficile
int MENU2(SDL_Window *window, SDL_Renderer *renderer,SDL_Surface * surface)
{
    SDL_Event event;
    int QUIT = 0;
    
    SDL_Texture *t;
    SDL_Rect rect;
    rect.x = 0;
    rect.y = 0;
    rect.w = WINDOW_WIDTH;
    rect.h = WINDOW_HEIGHT;

    surface = IMG_Load("image/menu2.jpg");
    t = SDL_CreateTextureFromSurface(renderer, surface);
    int choice;
    while (QUIT == 0)
    {

        while (SDL_PollEvent(&event))
        {

            if (event.type == SDL_QUIT)
            {
                QUIT = 1;
            }

            if (event.type == SDL_KEYDOWN)
            {
                switch (event.key.keysym.sym)
                {
                //Fleche gauche FACILE
                case SDLK_LEFT:
                    choice = 1;
                    QUIT = 1;
                    break;
                //Fleche droite DIFFICILE
                case SDLK_RIGHT:
                    choice = 0;
                    QUIT = 1;
                    break;
                default:
                    break;
                }
            }
        }
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, t, &rect, &rect);        
        SDL_RenderPresent(renderer);
    }

    return choice;
}

int MENU3(SDL_Window *window, SDL_Renderer *renderer,int resultat, SDL_Surface * surface)
{
    SDL_Event event;
    int QUIT = 0;
    
    SDL_Texture *t;

    SDL_Rect rect;
    rect.x = 0;
    rect.y = 0;
    rect.w = WINDOW_WIDTH;
    rect.h = WINDOW_HEIGHT;


    if(resultat == 1){
        surface = IMG_Load("image/menuGO.jpg");
    }else{
        surface = IMG_Load("image/menuV.jpg");
    }
    t = SDL_CreateTextureFromSurface(renderer, surface);
    int choice;
    while (QUIT == 0)
    {

        while (SDL_PollEvent(&event))
        {

            if (event.type == SDL_QUIT)
            {
                QUIT = 1;
            }

            if (event.type == SDL_KEYDOWN)
            {
                switch (event.key.keysym.sym)
                {
               
                //Quit
                case SDLK_q:
                    QUIT = 1;
                    choice = QUIT;
                    break;
                case SDLK_SPACE:
                    QUIT = 1;
                    choice = 0;
                    break;
                default:
                    break;
                }
            }
        }
        SDL_RenderClear(renderer);
        SDL_RenderCopy(renderer, t, &rect, &rect);
        SDL_RenderPresent(renderer);

    }


    return choice;
}







//return 0 pour une victoire 1 pour une defaite
int PLAY(SDL_Window *window, SDL_Renderer *renderer,int mode,SDL_Surface * surface)
{
    // ------VARIABLES------ //

    //test de fin de partie pour la boucle de simulation
    int GAME_OVER = 0;

    //taille fenetre

    int orientation = 0;
    int move = 0;
    //------VARIABLES SDL------//
    SDL_Event event;

    //creation du renderer

    // INITIALISATION JEU //
    //Creation de la carte
    char **carte = Creer_Carte(90, 90);

    //Creation de la carte des objets dynamiques
    char **carteDynamique = Allocation_Dynamique_Matrice(90, 90);
    Remplir_Carte_Dynamique(90, 90, carteDynamique);
    //Creation du joeur en position 400 800
    listeTank * joueur = Init_ListeTank();
    listeBalle * ballesJoueur = Init_ListeBalle();

    Ajouter_Tank_Fin(400, 600, joueur,0,carteDynamique,ballesJoueur);
    
    //Tableau des enemies//
    /* Le tableau d'enemie contient le nombre de tank a creer ainsi que leur niveau */
    //Mode facile : 15 tank -> 9 niveau leger, 4 niveau mouyen , 2 niveau 3 niveau lourd

    int tabTankFacile[15] = {0,0,2,0,0,0,0,2,0,1,1,1,1,2,2};
    int tabTankDifficile[30] = {0,1,2,0,0,0,0,0,0,1,1,1,1,0,0,0, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2};
    int * tabTank;
    int nb_enemy;
    if (mode == 0 ){

        nb_enemy=30;
        tabTank = tabTankDifficile;

    } else {
        nb_enemy=15;
        tabTank = tabTankFacile;
    }

    int indexTank = 0;
    listeTank *enemy = Init_ListeTank();
    listeBalle *ballesEnemy = Init_ListeBalle();



    //----Texture-----//
    SDL_Texture * textures[3];
    Init_Texture(surface,renderer,textures);



    //------ BOUCLE DE SIMULATION ------//
    Uint32 prevTime30, prevTime120, prevTime2000 = 0;
    Uint32 currentTime30, currentTime120, currentTime2000 = 0;

    while (GAME_OVER == 0)

    {

        while (SDL_PollEvent(&event))
        {
            move = 0;
            if (event.type == SDL_QUIT)
            {
                GAME_OVER = 1;
            }

            if (event.type == SDL_KEYDOWN)
            {

                switch (event.key.keysym.sym)
                {
                case SDLK_DOWN:
                    move = 1;
                    orientation = 0;
                    break;
                case SDLK_RIGHT:
                    move = 1;
                    orientation = 1;
                    break;
                case SDLK_UP:
                    move = 1;
                    orientation = 2;
                    break;
                case SDLK_LEFT:
                    move = 1;
                    orientation = 3;
                    break;
                case SDLK_SPACE:
                    shoot(joueur->debut, orientation);
                    break;
                case SDLK_q:
                    //Si on quite: game over
                    return 1;
                    break;
                default:
                    move = 0;
                    break;
                }
            }
        }

        currentTime30 = SDL_GetTicks();
        if (currentTime30 - prevTime30 > 30) /* Si 30 ms se sont écoulées depuis le dernier tour de boucle */
        {
            if (move == 1 && ListeTank_vide(joueur) == 0)
            {   
                
                Bouger_Tank(joueur->debut, orientation, carte, carteDynamique, 1);
            }
            bouger_balles(ballesJoueur, carte, enemy, carteDynamique,1);
            GAME_OVER = bouger_balles(ballesEnemy, carte, joueur, carteDynamique,0);
            if(GAME_OVER == 1) return GAME_OVER;
            prevTime30 = currentTime30; /* Le temps "actuel" devient le temps "precedent" pour nos futurs calculs */

            //ACTIONS
            currentTime120 = SDL_GetTicks();
            if (currentTime120 - prevTime120 > 120) /* Si 120 ms se sont écoulées depuis le dernier tour de boucle */
            {
                Bouger_Tank_Liste(enemy, carte, carteDynamique);
                prevTime120 = currentTime120; /* Le temps "actuel" devient le temps "precedent" pour nos futurs calculs */
            }
            currentTime2000 = SDL_GetTicks();
            if (currentTime2000 - prevTime2000 > 2000) /* Si 5000 ms se sont écoulées depuis le dernier tour de boucle */
            {
                if (Test_Carte_libre(carteDynamique, 10, 10) == 1 && indexTank < nb_enemy)
                {
                    Apparaitre_Tank_Enemy_Sur_Map(tabTank, enemy, indexTank, carteDynamique,ballesEnemy);
                    indexTank += 1;
                }

                prevTime2000 = currentTime2000; /* Le temps "actuel" devient le temps "precedent" pour nos futurs calculs */
            }
        }

        SDL_RenderClear(renderer);

        afficher_tanks(enemy, renderer,textures,0);
        afficher_tanks(joueur, renderer, textures,1);
        
        afficher_balles(ballesJoueur,textures, renderer,1); //1: Joueur 
        afficher_balles(ballesEnemy,textures ,renderer,0);  //0: Enemy

        Afficher_Carte(renderer, WINDOW_HEIGHT / 10, WINDOW_WIDTH / 10, carte, textures);

        SDL_SetRenderDrawColor(renderer,30, 39, 46,255);
        SDL_SetRenderTarget(renderer, NULL);
        SDL_RenderPresent(renderer);
    
    if(ListeTank_vide(enemy)==1 && indexTank == nb_enemy){
        return GAME_OVER;
    }
    if (ListeTank_vide(joueur)==1)
        return 1;
    }

}

int main(int argc, char **argv)
{

    //------INITIALISATION SDL------//

    // initialisation du mode video SDL

    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
    { //test echec d'initialisation
        fprintf(stderr, "Échec de l'initialisation de la SDL (%s)\n", SDL_GetError());
        return -1;
    }


    // Creation de la fenetre
    SDL_Window *window = SDL_CreateWindow("BATTLE TANK", 0,
                                          0,
                                          WINDOW_HEIGHT,
                                          WINDOW_WIDTH,
                                          SDL_WINDOW_SHOWN);
                                          // load WAV file
 
 
    
    
    // open musique jeu

	if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 1024 ) == -1 ) 
		return -1; 


    Mix_Chunk * bip = NULL;
    Mix_Music * music = NULL;
    music = Mix_LoadMUS("Sons/music.wav");
    bip = Mix_LoadWAV("Sons/bip.wav");

    if (music == NULL)
		return -1;
    //Creation du renderer
    SDL_Renderer *renderer = SDL_CreateRenderer(window,
                                                -1,
                                                SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

    SDL_Surface * surface;
    //Mise en place du systeme aléatoire
    srand(time(NULL));
    //Lancement Menu Acceuil
		
    int QUIT = 0;
    int mode,resultat,replay;

    if ( Mix_PlayMusic( music, -1) == -1 )
        return -1;

    while(QUIT == 0){
        
       

        
        //Ecran Acceuil
        MENU1(window, renderer,surface);

        //Selection de la difficulté
        mode = MENU2(window,renderer,surface);

        //Lancement du jeu
        resultat = PLAY(window, renderer,mode,surface);

        //Gagner ou Perdu et replay

        SDL_Delay(1000);
        replay = MENU3(window,renderer,resultat,surface);
        QUIT = replay;






            
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);

    }
    

    
	// quit SDL_mixer
	Mix_FreeChunk(bip);
	Mix_FreeMusic(music);
	Mix_CloseAudio();
    SDL_FreeSurface(surface);

    //Fermeture de la fenetre
    SDL_DestroyWindow(window);
    SDL_Quit();

    return 0;
}
